using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillTree
{
    private static SkillTree INSTANCE = null;

    private Skill[] skills;

    class Skill
    {
        private string name;
        private int lvl;

        public string Name { get => name; set => name = value; }
        public int Lvl { get => lvl; set => lvl = value; }
    }

    public static void SetValue(SkillTree v)
    {
        INSTANCE = v;
    }

    public static SkillTree GetAndClearValue()
    {
        SkillTree v = INSTANCE;
        INSTANCE = null;
        return v;
    }
}
