using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpLogger : MonoBehaviour
{


    void Start()
    {
        //int expGained = PlayerPrefs.GetInt(PrefsManager.PREF_NAME);
        int expGained = ReadOnceValue.GetAndClearValue();
        Debug.Log(expGained);
    }
}
