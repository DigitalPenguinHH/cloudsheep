using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScorePlus : MonoBehaviour
{
    public TextMeshProUGUI scoreLabel;

    [SerializeField]
    private int points = 0;
    public int Points { get => points; set => points = value; }

    public void Add(int points)
    {
        Points += points;
        ReadOnceValue.SetValue(Points);
        scoreLabel.text = string.Format("Points: {0:0000}", Points);
    }
}
