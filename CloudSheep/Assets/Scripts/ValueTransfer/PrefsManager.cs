using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PrefsManager : MonoBehaviour
{

    public const string PREF_NAME = "minigame.exp.gained";

    void Awake()
    {
        
    }

    public void SavePrefs()
    {
        int points = GameObject.Find("PlusButton").GetComponent<ScorePlus>().Points;
        PlayerPrefs.SetInt(PREF_NAME, points);
        SceneManager.LoadScene("PlayerPrefsScene2");
    }
}
