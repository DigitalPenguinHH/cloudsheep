using System;
using UnityEngine;

public class ReadOnceValue : MonoBehaviour
{
    const int UNDEFINED = -1;

    [SerializeField]
    private static int value = UNDEFINED;

    public static void SetValue(int v)
    {
        value = v;
    }

    public static int GetAndClearValue()
    {
        int v = value;
        value = UNDEFINED;
        return v;
    }
}
